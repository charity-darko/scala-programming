package project
import munit.FunSuite

class LinkedListTest extends FunSuite{
  test("New List with no value should return an empty list") {
    val myList = LList()
    assertEquals(myList.isEmpty, true)
  }

  test("List with some values should not return empty list") {
    val myList = LList(1, "charity", 2, 3, 4)
    assertEquals(myList.isEmpty, false)
  }

  test("List with no values should return length of 0") {
    val myList = LList()
    assertEquals(myList.length, 0)
  }

  test("List with values should return length of items in the list") {
    val myList = LList(1, "charity", 2, 3, 4)
    assertEquals(myList.length, 5)
  }

  test("List with no values should return []") {
    val myList = LList()
    assertEquals(myList.toString, "[]")
  }

  test("List with values 1, charity, 2, 3, 4 should return [1, charity, 2, 3, 4]") {
    val myList = LList(1, "charity", 2, 3, 4)
    assertEquals(myList.toString, "[1, charity, 2, 3, 4]")
  }

  test("When darko is added to List(1, charity, 2, 3, 4) should return [darko, 1, charity, 2, 3, 4]") {
    val myList = LList(1, "charity", 2, 3, 4)
    assertEquals(myList.::("darko").toString, "[darko, 1, charity, 2, 3, 4]")
  }

  test("List(1, 2, 3, 4, 5) ++ List(6, 7, 8) should return List(1, 2, 3, 4, 5, 6, 7, 8)") {
    val list1 = LList(1, 2, 3, 4, 5)
    val list2 = LList(6, 7, 8)

    val result = LList(1, 2, 3, 4, 5, 6, 7, 8)

    assertEquals((list1 ++ list2).toString, result.toString)
  }

  test("List(1, charity, 2, 3, 4) ++ List(6, 7, 8) should return List(1, 2, 3, 4, 5, 6, 7, 8)") {
    val list1 = LList(1, "charity", 2, 3, 4)
    val list2 = LList(6, 7, 8)

    val result = LList(1, "charity", 2, 3, 4, 6, 7, 8)

    assertEquals((list1 ++ list2).toString, result.toString)
  }

  test("Given LList(1, 2, 3, 4) apply map with value * 3 should return LList(3, 6, 9, 12)") {
    val myList = LList(1, 2, 3, 4)
    val result = myList.map(f => f * 3)

    assertEquals(result.toString, "[3, 6, 9, 12]")
  }

  test("FlatMap") {

  }

  test("Filter") {
    val myList = LList(1, 2, 3, 4)
    val result = myList.filter(_ % 2 == 0)

    assertEquals(result.toString, "[2, 4]")
  }
}
