package project

import scala.annotation.targetName

sealed trait LList[+A]{
  def isEmpty: Boolean
  def length: Int
  def stringRepresentation: String
  override def toString: String = "[" + stringRepresentation + "]"
  @targetName("Add an element to the front of the list")
  def ::[B>:A](newElement:B) : LList[B]
  @targetName("Concatenates two lists together to make a single list")
  def ++[B>:A](otherList: LList[B]) : LList[B]
  def map[B >: A](f: (B) => B) : LList[B]
  def flatMap[B >: A](f: (A) => LList[B]) : LList[B]
  def filter[B >: A](f: (B) => Boolean) : LList[B]
  def withFilter[B >: A](f: A => Boolean): LList[B]
  def forEach[B >: A](f: A => B): Unit
}

case class Empty[A]() extends LList[A]{
  override def isEmpty: Boolean = true

  override def length: Int = 0

  override def stringRepresentation: String = ""

  @targetName("Add an element to the front of the list")
  override def ::[B >: A](newElement: B): LList[B] = NonEmpty(newElement, this)

  @targetName("Concatenates two lists together to make a single list")
  override def ++[B >: A](otherList: LList[B]): LList[B] = otherList

  override def map[B >: A](f: (B) => B): LList[B] = this

  override def flatMap[B >: A](f: A => LList[B]): LList[B] = this

  override def filter[B >: A](f: (B) => Boolean): LList[B] = this

  override def withFilter[B >: A](f: A => Boolean): LList[B] = this

  override def forEach[B >: A](f: A => B): Unit = ()
}

case class NonEmpty[+A](head : A, tail : LList[A]) extends LList[A]{
  override def isEmpty: Boolean = false

  override def length: Int = 1 + tail.length

  override def stringRepresentation: String = if (tail.isEmpty) "" + head else head.toString + ", " + tail.stringRepresentation

  @targetName("Add an element to the front of the list")
  override def ::[B >: A](newElement: B): LList[B] = NonEmpty(newElement, this)

  @targetName("Concatenates two lists together to make a single list")
  override def ++[B >: A](otherList: LList[B]): LList[B] = NonEmpty(head, tail ++ otherList)

  override def map[B >: A](f: (B) => B): LList[B] = NonEmpty(f(head), tail.map(f))

  override def flatMap[B >: A](f: A => LList[B]): LList[B] = f(head) ++ tail.flatMap(f)

  override def filter[B >: A](f: (B) => Boolean): LList[B] = if(f(head)) NonEmpty(head, tail.filter(f)) else tail.filter(f)

  override def withFilter[B >: A](f: A => Boolean): LList[B] = this.filter(f)

  override def forEach[B >: A](f: A => B): Unit = {
    f(head); tail.forEach(f)
  }

}

object LList{
  def apply[A](items : A*):LList[A] = if(items.isEmpty) Empty[A]() else NonEmpty(items.head, apply(items.tail : _*))
}
