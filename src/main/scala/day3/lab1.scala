package day3

object lab1 {

  def main(string: Array[String]): Unit =
    operation("add")(2,3)
    operation("subtract")(2,3)
    operation("power")(2,3)


    def operation(action: String): (Int, Int) => Unit =
      action match
        case "add" => (a: Int, b: Int)
          => println(s"Adding $a and $b is ${a + b}")
        case "subtract" => (a: Int, b: Int)
          => println(s"Subtracting $b from $a is ${a - b}")
        case "power" => (a: Int, b: Int)
          => println(s"$a power $b is ${Math.pow(a, b).toInt}")
        case _ => (a: Int, b: Int)
          => println("Not sure of what you want to achieve")
}

