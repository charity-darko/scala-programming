package day3

object lab2 {
  def main(string: Array[String]): Unit = {
    val list = new java.io.File(".").listFiles()

    // Question 1
    println(list.mkString("\n"))
    println()


    // Question 2
    val myFiles = list.filter(f => f.isFile)
    println("Files")
    myFiles.map(println)

    println()

    val myDirectories = list.filter(d => d.isDirectory)
    println("Directories")
    myDirectories.map(println)

    println()


    // Question 3
    println("Filename, Size")
    myFiles.map(f => println(f.getName() + " " + f.length()))


    // Question 4
    println()
    val smallest = myFiles.sortWith((f1, f2) => f1.length() < f2.length())
    println("Smallest 10")
    smallest.take(10).map(f => println(f.getName() + " " + f.length()))

    println()
    val largest = myFiles.sortWith((f1, f2) => f1.length() > f2.length())
    println("Largest 10")
    largest.take(10).map(f => println(f.getName() + " " + f.length()))


    // Question 5
    println()
    println("Smallest 10 with Map")
    val fileWithSize = myFiles.map(f => (f.getName, f.length()))
    val fileWithSizeMap = Map(fileWithSize: _*)
    println(fileWithSizeMap)


    //    println()
    //    val largest = myFiles.sortWith((f1, f2) => f1.length() > f2.length())
    //    println("Largest 10")
    //    largest.take(10).map(f => println(f.getName() + " " +

    println()


    // Question 6
    println()

    val arrangedByFirstLetter = list.groupBy(f => f.getName.charAt(0).toLower)
    println("Arranged By First Letter")
    arrangedByFirstLetter.map((key, value) => {
      print(key + "->[ ")
      value.map(x => print(x.getName + " "))
      println("]")
    })


  }

}
