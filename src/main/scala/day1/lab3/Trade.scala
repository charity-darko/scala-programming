package day1.lab3

class Trade (val id: String, val symbol: String, val quantity: Int, private var _price: Double) {
  require(_price >= 0)

  def price = _price

  def price_= (v: Double)  = {
    if(v >= 0) _price = v
  }

  def value = price * quantity

  override def toString: String = s"Trade(id=$id, symbol=$symbol, quantity=$quantity, price=$price)"
}

object Trade {
  def apply(id: String, sym: String, qty: Int, p: Double) = new Trade(id, sym, qty, p)
  def apply(id: String, sym: String, qty: Int) = new Trade(id, sym, qty, 0)
}



/* using case class */
//case class Trade (val id: String, val symbol: String, val quantity: Int, private var _price: Double) {
//  def price = _price
//
//  def price_= (v: Double)  = {
//    if(v >= 0) _price = v
//  }
//
//  def value = price * quantity
//}



