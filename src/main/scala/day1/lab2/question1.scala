package day1.lab2

object question1 {
  def main(args: Array[String]): Unit = {
    println(celsiusToFahrenheit(23))
  }
}

def celsiusToFahrenheit(temperature: Float): Float = (temperature * 9 / 5) + 32