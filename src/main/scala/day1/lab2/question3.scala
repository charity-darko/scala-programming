package day1.lab2
import scala.io.Source

object question3 {
  def main(args: Array[String]): Unit =
    val filePath = "src/resources/password_file.txt"
    extractFieldsGivenFile(filePath)

}

def extractFieldsGivenFile(filePath: String): Unit =
  val bufferedSource = Source.fromFile(filePath)
  var index = 1
  for (line <- bufferedSource.getLines) {
    println(s"Line $index")
    println("----------")

    var location = 1
    line.split(":").foreach(x => {
      location match {
        case 1 => print("username: ")
        case 2 => print("password: ")
        case 3 => print("userid: ")
        case 4 => print("groupid: ")
        case 5 => print("description: ")
        case 6 => print("directory: ")
        case 7 => print("shell: ")
        case _ => println("Unknown")
      }
      print(x)
      println("\n")
      location = location + 1
    })
    index = index + 1
  }