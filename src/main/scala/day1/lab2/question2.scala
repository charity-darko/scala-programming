package day1.lab2

import java.util.Date

object question2 {
  def main(args: Array[String]): Unit = {
    stringToDate("01/02/15")
  }
}

def stringToDate(arg: String): Unit =
  val individualUnits = arg.split("/")
  val day = individualUnits.apply(0)
  val month = individualUnits.apply(1)
  val year = individualUnits.apply(2)

  day match
    case "01" => print("1st ")
    case "21" | "31" => print(day + "st ")
    case "02" => print("2nd ")
    case "22"=> print(day + "nd ")
    case "03" => print("3rd ")
    case "23" => print(day + "rd ")
    case _ => print(day + "th ")

  month match
    case "01" => print("January")
    case "02" => print("February")
    case "03" => print("Match")
    case "04" => print("April")
    case "05" => print("May")
    case "06" => print("June")
    case "07" => print("July")
    case "08" => print("August")
    case "09" => print("September")
    case "10" => print("October")
    case "11" => print("November")
    case "12" => print("December")
    case _ => print("Not sure of the month")

  print(" 20" + year)