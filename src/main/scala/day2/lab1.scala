package day2

case class Customer(val typeOfMembership: String, val name: String, val age: Int)

object Library {

  def main(args: Array[String]): Unit = {
    val c1 = Customer("Junior member", "Lisa", age = 21)
    val c2 = Customer("Regular member", "Kezia", age = 2)
    val c3 = Customer("Senior Citizen member", "Gilbert", age = 26)

    val customer = c3

    customer.typeOfMembership match {
      case "Junior member" => println(s"Hello ${customer.name}, you're a ${customer.typeOfMembership} and you can borrow up to 10 ")
      case "Regular member" => println(s"Hello ${customer.name}, you're a ${customer.typeOfMembership} and you can borrow up to 25 ")
      case "Senior Citizen member" => println(s"Hello ${customer.name}, you're a ${customer.typeOfMembership} and you can borrow up to 100 ")
      case _ => println("Not sure of the membership type")
    }
  }
}
