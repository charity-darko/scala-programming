package day2.lab2

class EquityTrade(id: String, val symbol: String, val quantity: Int, price: Double)
  extends Trade(id, price) {
  def value = price * quantity

  override def isExecutable(): Boolean = true

  //override def toString(): String = s"id=$id, symbol=$symbol, ${super.toString()}"
  override def toString(): String = s"${super.toString()}"
}

