package day2.lab2

abstract class Trade (val id: String, private var _price: Double) {
  def price = _price

  def price_= (v: Double)  = {
    if(v >= 0) _price = v
  }


  def isExecutable(): Boolean

  override def toString: String = s"id=$id, price=$price"
}

