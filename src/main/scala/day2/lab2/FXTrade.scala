package day2.lab2

class FXTrade(id: String, price: Double) extends Trade(id, price){
  override def isExecutable(): Boolean = false
}