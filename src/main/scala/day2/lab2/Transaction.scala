package day2.lab2

class Transaction(id: String, symbol: String, quantity: Int, price: Double)
  extends EquityTrade(id, symbol, quantity, price) with FeePayable with Taxable{
  def amount(): Double = {
    val total = value + fee
    total + tax(total)
  }
}

