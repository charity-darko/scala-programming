package day2.lab2

trait Taxable {
  private val rate = 12.5
  def tax(amount: Double) = amount * (rate/100)
}

